<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            
            'name'=>'shachar nisanov',
            'email'=>'shachar@shachar.com',
            'password'=>Hash::make('12345678'),
        ],
        [
            'name'=>'sima zion',
            'email'=>'sima @sima.com',
            'password'=>Hash::make('12345678'),
       ],
       [
        'name'=>'lior a',
        'email'=>'liora@liora.com',
        'password'=>Hash::make('12345678'),
        ],
        
        ]);

    }
}
